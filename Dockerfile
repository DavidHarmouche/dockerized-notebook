FROM jupyter/datascience-notebook

COPY ./app /app

WORKDIR /app

RUN pip install --no-cache-dir -r requirements.txt  \
    && python -m spacy download en_core_web_sm


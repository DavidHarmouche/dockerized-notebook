# Using Notebooks inside a docker container
## "But it worked on my machine"

This Repository showcases the use of docker for collaborative work in jupyter Notebooks.
Reproducability tends to be major problem, when trying to run a coworker's notebook.
The use of a containerized notebook allows data scientists to bypass all dependency and package problems.

This notebook uses many data science libraries, so if you tried using it on your machine, you'd have to do a lot of package installing first.
To get started with docker simply clone the git repo (or pull) and run:
```
docker-compose up --build
```
(This might take a while to download and build the first time)
You will find a link and token to the notebook in your terminal (it's set to localhost:8888)

As you will see, all relevant files in the repo can be found within the container. Since the container is be bind mounted to your repo, all changes made to any file within the container will be mirrored on the host, so you can simply push your local repo and nothing gets left behind. The same goes for pulling. You might want to rebuild your container after pulling to reflect potential changes to the environment.

If you need more packages, add them to the requirements.txt file and rebuild your image. 
This way, when pushed, everyone can automatically execute your notebooks when they rebuild their image.
